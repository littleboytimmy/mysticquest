Although I think that this file will be read by only people that I know well
and, therefore, it is fairly unneeded, I guess I will be pedantic and write it
anyways. Within this project, please make sure to follow pip 8 as closely as
possible to keep the code as clean as possible. Also, please try to use the most
functional programming techniques that you can while contributing to this
project (and in life).